
# -*- coding: cp1252 -*-
import tkinter as Tk

root = Tk.Tk()   ## Fen�tre principale

def Affiche(): tkMessageBox.showinfo("Exemple d'un Menu Tkinter")
def About(): tkMessageBox.showinfo("A propos", "Version 1.0")
    
mainmenu = Tk.Menu(root)  ## Barre de menu
menuExample = Tk.Menu(mainmenu)  ## Menu fils menuExample
menuExample.add_command(label="Affiche", command=Affiche)  ## Ajout d'une option au menu fils menuFile
menuExample.add_command(label="Quitter", command=root.quit)

menuHelp = Tk.Menu(mainmenu) ## Menu Fils
menuHelp.add_command(label="A propos", command=About)

mainmenu.add_cascade(label = "Exemple", menu=menuExample)
mainmenu.add_cascade(label = "Aide", menu=menuHelp)

root.config(menu = mainmenu)

root.mainloop()
