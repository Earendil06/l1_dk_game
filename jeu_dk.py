from tkinter import *
import os
from random import choice

class Menu :
    """fenetre de menu principal"""
    
    def __init__(self):
        global var
        
        self.fenetre = Tk()
        self.fenetre.title('Jeu de Donkey Kong')
        self.fenetre.resizable(0,0)
        var = StringVar()
        
        #difficulte :
        label_diff = Label(self.fenetre, text = 'Veuillez choisir votre difficulté :', fg = '#01d758')
        label_diff.grid(row = 0, column = 0)
        mode_facile = Radiobutton(self.fenetre, text = 'Facile (15x15)', variable = var, value = 'facile') 
        mode_moyen = Radiobutton(self.fenetre, text = 'Moyen (20x20)', variable = var, value = 'moyen') 
        mode_difficile = Radiobutton(self.fenetre, text = 'Difficile (40x20)', variable = var, value = 'difficile') 
        mode_facile.grid(row = 1, column = 0)
        mode_moyen.grid(row = 1, column = 1)
        mode_difficile.grid(row = 1, column = 2)
        
        #canvas :
        canvas = Canvas(self.fenetre)
        canvas.configure(width = 450, height = 450)
        fond = PhotoImage(file = 'images/accueil.gif')
        canvas.create_image(0,0, anchor = NW, image = fond)
        canvas.grid(row = 2, column = 0, columnspan = 3)
        canvas.bind('<Button-1>', self.click)

        
        self.fenetre.mainloop()
        exit(0)

    def click(self,event):
        if (event.x > 170 and event.x < 280) and (event.y > 380 and event.y < 415) :
            self.fichier()

    def fichier(self,difficulte = None):
        global niveau
        difficulte = var.get()
        try : os.chdir('niveaux/{}'.format(difficulte))
        except : pass
        f_in = open(choice(os.listdir()), 'r')
        niveau = f_in.read().split('\n')
        f_in.close()
        os.chdir('../../')
        self.fenetre.destroy()
        Fenetre(difficulte)
        
        
class Gagne :
    """Fenetre apparaissant lors de la victoire"""

    def __init__(self):
        self.fenetre = Toplevel()
        self.fenetre.title('Félicitations')
        
        msg = Message(self.fenetre)
        msg.configure(text = 'Bravo, vous avez terminé le niveau !')
        msg.grid(row = 0, column = 0, columnspan = 2)
        
        quitter = Button(self.fenetre)
        quitter.configure(text = 'Quitter', command = self.fenetre.destroy)
        quitter.grid(row = 1, column = 0)
        
    
class Fenetre :
    """Fenetre de jeu"""

    def __init__(self,difficulte = 'facile'):
        global coordonnees_arrivee, coordonnees_depart, zone, dk, dk_droite, dk_gauche, dk_haut, dk_bas, stop
        
        self.difficulte = difficulte
        self.fenetre = Tk()
        self.fenetre.title('Jeu de Donkey Kong')
        self.fenetre.resizable(0,0)

        fond_zone = PhotoImage(file = 'images/fond_{}.gif'.format(difficulte))
        mur_zone = PhotoImage(file = 'images/mur.gif')
        depart_zone = PhotoImage(file = 'images/depart.gif')
        arrivee_zone = PhotoImage(file = 'images/arrivee.gif')
        dk_droite = PhotoImage(file = 'images/dk_droite.gif')
        dk_gauche = PhotoImage(file = 'images/dk_gauche.gif')
        dk_haut = PhotoImage(file = 'images/dk_haut.gif')
        dk_bas = PhotoImage(file = 'images/dk_bas.gif')

        bout_retour = Button(self.fenetre)
        bout_retour.configure(text = 'Retour', command = self.retour)
        bout_retour.grid(row = 2, column = 0)

        bout_quit = Button(self.fenetre)
        bout_quit.configure(text = 'Quitter', command = self.quitter)
        bout_quit.grid(row = 2, column = 1)

        try : zone.delete(ALL)
        except : None
        zone = Canvas(self.fenetre)
        zone.grid(row = 1, column = 0, columnspan = 2)
        if self.difficulte == 'facile' :
            zone.configure(width = 450, height = 450) #15*15
        elif self.difficulte == 'moyen' :
            zone.configure(width = 600, height = 600) #20*20
        elif self.difficulte == 'difficile' :
            zone.configure(width = 1200, height = 600) #40*20
        zone.create_image(0, 0, anchor = NW, image = fond_zone)

        #creation des blocs :
        j = 0
        for ligne in niveau :
            i = 0
            for element in ligne :
                if element == 'm' :
                    zone.create_image(30 * i, 30 * j, anchor = NW, image = mur_zone)
                if element == 'd' :
                    zone.create_image(30 * i, 30 * j, anchor = NW, image = depart_zone)
                    coordonnees_depart = (i,j)
                if element == 'a' :
                    zone.create_image(30 * i, 30 * j, anchor = NW, image = arrivee_zone)
                    coordonnees_arrivee = (j,i)
                i += 1
            j += 1
        ###################
        #creation du perso :
        dk = zone.create_image(coordonnees_depart[0] * 30, coordonnees_depart[1] * 30, anchor = NW, image = dk_droite)
        self.fenetre.bind('<Right>', self.droite)
        self.fenetre.bind('<Left>', self.gauche)
        self.fenetre.bind('<Up>', self.haut)
        self.fenetre.bind('<Down>', self.bas)
        stop = 0
        
        self.fenetre.mainloop()
        exit(0)

    def retour(self):
        self.fenetre.destroy()
        Menu()

    def quitter(self):
        self.fenetre.destroy()

    def test(self,ca,cd):
        global stop
        if ca == cd :
            stop = 1
            Gagne()
            
            

    def droite(self,event):
        global coordonnees_depart,dk
        if coordonnees_depart[1] == len(niveau[0])-1 or stop == 1 :
            return None
        if not niveau[coordonnees_depart[0]][coordonnees_depart[1]+1] == 'm' :
            zone.delete(dk)
            coordonnees_depart = (coordonnees_depart[0],coordonnees_depart[1]+1)
            dk = zone.create_image(coordonnees_depart[1]*30,coordonnees_depart[0]*30, anchor = NW, image = dk_droite)
            self.test(coordonnees_depart,coordonnees_arrivee)
            
    def gauche(self,event):
        global coordonnees_depart,dk
        if coordonnees_depart[1] == 0 or stop == 1 :
            return None
        if not niveau[coordonnees_depart[0]][coordonnees_depart[1]-1] == 'm' :
            zone.delete(dk)
            coordonnees_depart = (coordonnees_depart[0],coordonnees_depart[1]-1)
            dk = zone.create_image(coordonnees_depart[1]*30,coordonnees_depart[0]*30, anchor = NW, image = dk_gauche)
            self.test(coordonnees_depart,coordonnees_arrivee)
            
    def haut(self,event):
        global coordonnees_depart,dk
        if coordonnees_depart[0] == 0 or stop == 1 :
            return None
        if not niveau[coordonnees_depart[0]-1][coordonnees_depart[1]] == 'm' :
            zone.delete(dk)
            coordonnees_depart = (coordonnees_depart[0]-1,coordonnees_depart[1])
            dk = zone.create_image(coordonnees_depart[1]*30,coordonnees_depart[0]*30, anchor = NW, image = dk_haut)
            self.test(coordonnees_depart,coordonnees_arrivee)
            
    def bas(self,event):
        global coordonnees_depart,dk
        if coordonnees_depart[0] == len(niveau)-1 or stop == 1 :
            return None
        if not niveau[coordonnees_depart[0]+1][coordonnees_depart[1]] == 'm' :
            zone.delete(dk)
            coordonnees_depart = (coordonnees_depart[0]+1,coordonnees_depart[1])
            dk = zone.create_image(coordonnees_depart[1]*30,coordonnees_depart[0]*30, anchor = NW, image = dk_bas)
            self.test(coordonnees_depart,coordonnees_arrivee)

Menu()
